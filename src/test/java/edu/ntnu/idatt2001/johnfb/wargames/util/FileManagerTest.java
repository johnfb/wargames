package edu.ntnu.idatt2001.johnfb.wargames.util;

import edu.ntnu.idatt2001.johnfb.wargames.objects.simulation.Army;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import java.io.File;
import java.io.IOException;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Class used to test the FileManager
 * @author johnfb
 * @version 1.0
 */
public class FileManagerTest {
    private Army army1;

    @BeforeEach
    public void initEach() {
        army1 = new Army("Test Army");

        army1.addAll(UnitFactory.createUnits("InfantryUnit", "Infantry", 100, 100));
        army1.addAll(UnitFactory.createUnits("RangedUnit", "Ranged", 50, 50));
        army1.addAll(UnitFactory.createUnits("CavalryUnit", "Cavalry", 75, 50));
        army1.addAll(UnitFactory.createUnits("CommanderUnit", "Commander", 150, 10));
    }

    @Nested
    @DisplayName("Tests if an army can be written and read")
    public class ReadWriteTests {
        @Test
        @DisplayName("Tests if the application can create new files from armies")
        public void writeArmyTest() {
            try {
                FileManager.writeArmy(army1, "src/test/java/edu/ntnu/idatt2001/johnfb/wargames/util/armyFile");
            } catch (IOException e) {
                fail(e.getMessage());
                return;
            }

            File file = new File("src/test/java/edu/ntnu/idatt2001/johnfb/wargames/util/armyFile.csv");

            assertTrue(file.exists());
            assertEquals("armyFile.csv", file.getName());
            assertTrue(file.delete());

            Army army2 = new Army("Epic army");
            try {
                FileManager.writeArmy(army2, "src/test/java/edu/ntnu/idatt2001/johnfb/wargames/util/epicFile");
            } catch (IOException e) {
                fail(e.getMessage());
                return;
            }

            File file2 = new File("src/test/java/edu/ntnu/idatt2001/johnfb/wargames/util/epicFile.csv");

            assertTrue(file2.exists());
            assertEquals("epicFile.csv", file2.getName());
            assertTrue(file2.delete());
        }

        @Test
        @DisplayName("Tests exception handling when writing a file")
        public void writeExceptionHandlingTest() {
            try {
                FileManager.writeArmy(army1, "");
            } catch (IOException e) {
                assertEquals("You must enter a filename.", e.getMessage());
            }
        }

        @Test
        @DisplayName("Tests if the application can read armies from files")
        public void readArmyTest() {
            try {
                FileManager.writeArmy(army1, "src/test/java/edu/ntnu/idatt2001/johnfb/wargames/util/armyFile");
            } catch (IOException e) {
                fail(e.getMessage());
            }

            File file = new File("src/test/java/edu/ntnu/idatt2001/johnfb/wargames/util/armyFile.csv");

            Army army2;
            try {
                army2 = FileManager.readArmy(file);
            } catch (IOException e) {
                fail(e.getMessage());
                return;
            }

            assertTrue(file.delete());

            assertEquals(army1.getName(), army2.getName());
            assertEquals(army1.getAllUnits().size(), army2.getAllUnits().size());
            assertEquals(army1, army2);
            assertEquals(100, army2.getInfantryUnits().size());
            assertEquals(50, army2.getRangedUnits().size());
            assertEquals(50, army2.getCavalryUnits().size());
            assertEquals(10, army2.getCommanderUnits().size());
        }

        @Test
        @DisplayName("Tests exception handling when reading a file")
        public void readExceptionHandlingTest() {
            File wrongFile = new File("src/test/java/edu/ntnu/idatt2001/johnfb/wargames/util/corruptArmyFile");
            try {
                FileManager.readArmy(wrongFile);
                fail("Tried to read a file that doesn't exist.");
            } catch (IOException e) {
                assertEquals("Only .csv files are supported.", e.getMessage());
            }

            File corruptFile = new File("src/test/java/edu/ntnu/idatt2001/johnfb/wargames/util/corruptArmyFile.csv");
            try {
                FileManager.readArmy(corruptFile);
                fail("Created an army with corrupted data.");
            } catch (IOException e) {
                assertEquals("The health value must be an integer.", e.getMessage());
            }

            File corruptFile2 = new File("src/test/java/edu/ntnu/idatt2001/johnfb/wargames/util/corruptArmyFile2.csv");
            try {
                FileManager.readArmy(corruptFile2);
                fail("Created an army with corrupted data.");
            } catch (IOException e) {
                assertEquals("File-data is invalid. Make sure the file is correctly formatted.", e.getMessage());
            }

            File corruptFile3 = new File("src/test/java/edu/ntnu/idatt2001/johnfb/wargames/util/corruptArmyFile3.csv");
            try {
                FileManager.readArmy(corruptFile3);
                fail("Created an army with corrupted data.");
            } catch (IOException e) {
                assertEquals("The unit type: 'EpicUnitOfMassDestruction' does not exist.", e.getMessage());
            }
        }
    }
}
