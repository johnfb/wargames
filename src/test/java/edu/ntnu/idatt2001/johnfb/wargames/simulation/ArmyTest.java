package edu.ntnu.idatt2001.johnfb.wargames.simulation;

import edu.ntnu.idatt2001.johnfb.wargames.objects.simulation.Army;
import edu.ntnu.idatt2001.johnfb.wargames.objects.units.*;
import edu.ntnu.idatt2001.johnfb.wargames.objects.units.unitTypes.InfantryUnit;
import edu.ntnu.idatt2001.johnfb.wargames.util.UnitFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class is used to test the Army-class
 * @author johnfb
 * @version 1.0
 */
public class ArmyTest {
    private Unit unit1;
    private Unit unit2;
    private Unit unit3;
    private Unit unit4;
    private Unit unit5;
    private Unit unit6;
    private Unit unit7;

    UnitFactory factory = new UnitFactory();

    @BeforeEach
    public void initEach() {
        unit1 = factory.createUnit("InfantryUnit", "Unit1", 100);
        unit2 = factory.createUnit("InfantryUnit", "Unit2", 100);
        unit3 = factory.createUnit("InfantryUnit", "Unit3", 100);
        unit4 = factory.createUnit("InfantryUnit", "Unit4", 100);
        unit5 = factory.createUnit("RangedUnit", "Unit5", 75);
        unit6 = factory.createUnit("CavalryUnit", "Unit6", 100);
        unit7 = factory.createUnit("CommanderUnit", "Unit7", 100);
    }

    @Nested
    @DisplayName("Army creation tests")
    public class CreateArmyTests {
        /**
         * Tests the creation of an army and adding units to an army.
         */
        @Test
        @DisplayName("Tests if an army can be created")
        public void armyCreationTest() {
            Army army1 = new Army("Army1");
            army1.add(unit1);
            army1.add(unit2);

            List<Unit> testList = new ArrayList<>();
            testList.add(unit1);
            testList.add(unit2);

            assertEquals(army1.getAllUnits(), testList);
            assertEquals(army1.getName(), "Army1");

            testList.add(unit3);
            Army army2 = new Army("Army2", testList);
            assertEquals(army2.getAllUnits(), testList);
        }

        @Test
        @DisplayName("Test exception handling")
        public void armyExceptionTest() {
            try {
                Army army = new Army("");
                fail("Method did not throw IllegalArgumentException as expected");
            } catch (IllegalArgumentException ex) {
                assertEquals("Name can't be blank", ex.getMessage());
            }
        }
    }

    @Nested
    @DisplayName("Army editing tests")
    public class ArmyEditingTests {
        /**
         * Tests if you can get a unit from the army by the getRandom-function.
         * Tests if you can remove a unit that doesn't exist in the army.
         */
        @Test
        @DisplayName("Tests if the units in the army can be modified")
        public void armyEditingTest() {
            Army army1 = new Army("Army1");
            army1.add(unit4);
            army1.add(unit3);
            army1.add(unit2);

            assertEquals(army1.getRandom().getClass(), InfantryUnit.class);

            army1.add(unit5);

            army1.remove(unit4);
            army1.remove(unit3);
            army1.remove(unit2);

            assertEquals(army1.getRandom(), unit5);

            try {
                army1.remove(unit6);
                fail("Method did not throw IllegalArgumentException as expected");
            } catch (IllegalArgumentException ex) {
                assertEquals(ex.getMessage(), "The unit is not in this army");
            }
        }

        /**
         * Tests the army name
         */
        @Test
        @DisplayName("Tests the army name value")
        public void armyNameTest() {
            try{
                Army testArmy = new Army("RedArmy");
                assertEquals("RedArmy", testArmy.getName());
            }catch (Exception e){
                fail("'armyNameTest' failed");
            }
        }
    }

    @Nested
    @DisplayName("Army unit-handling tests")
    public class GetUnitsFromArmyTests {
        /**
         * Tests the getInfantryUnits-method
         */
        @Test
        @DisplayName("Tests getInfantry")
        public void getInfantryUnitsTest() {
            ArrayList<Unit> targetList = new ArrayList<>();
            targetList.add(unit1);
            targetList.add(unit2);
            targetList.add(unit3);
            targetList.add(unit4);

            Army army1 = new Army("Army1");
            army1.add(unit1);
            army1.add(unit2);
            army1.add(unit5);
            army1.add(unit6);
            army1.add(unit3);
            army1.add(unit4);

            assertEquals(targetList, army1.getInfantryUnits());
        }

        /**
         * Tests the getCavalryUnits-method
         */
        @Test
        @DisplayName("Tests getCavalry")
        public void getCavalryUnitsTest() {
            ArrayList<Unit> targetList = new ArrayList<>();
            targetList.add(unit6);

            Army army1 = new Army("Army1");
            army1.add(unit1);
            army1.add(unit2);
            army1.add(unit5);
            army1.add(unit6);
            army1.add(unit3);
            army1.add(unit4);

            assertEquals(targetList, army1.getCavalryUnits());
        }

        /**
         * Tests the getRangedUnits-method
         */
        @Test
        @DisplayName("Tests getRanged")
        public void getRangedUnitsTest() {
            ArrayList<Unit> targetList = new ArrayList<>();
            targetList.add(unit5);

            Army army1 = new Army("Army1");
            army1.add(unit1);
            army1.add(unit2);
            army1.add(unit5);
            army1.add(unit6);
            army1.add(unit3);
            army1.add(unit4);

            assertEquals(targetList, army1.getRangedUnits());
        }

        /**
         * Tests the getCommanderUnits-method
         */
        @Test
        @DisplayName("Tests getCommander")
        public void getCommanderUnitsTest() {
            ArrayList<Unit> targetList = new ArrayList<>();
            targetList.add(unit7);

            Army army1 = new Army("Army1");
            army1.add(unit1);
            army1.add(unit2);
            army1.add(unit5);
            army1.add(unit7);
            army1.add(unit3);
            army1.add(unit4);

            assertEquals(targetList, army1.getCommanderUnits());
        }
    }
}
