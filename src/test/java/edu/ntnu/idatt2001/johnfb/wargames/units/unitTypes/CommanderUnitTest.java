package edu.ntnu.idatt2001.johnfb.wargames.units.unitTypes;

import edu.ntnu.idatt2001.johnfb.wargames.objects.units.Unit;
import edu.ntnu.idatt2001.johnfb.wargames.objects.units.unitTypes.CommanderUnit;
import edu.ntnu.idatt2001.johnfb.wargames.util.UnitFactory;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Class is used to test the CommanderUnit-class
 * @author johnfb
 * @version 1.0
 */
public class CommanderUnitTest {
    @Nested
    @DisplayName("Commander Constructor Tests")
    public class TestsForInfantryConstructor {
        @Test
        @DisplayName("Commander creation test")
        public void unitCreationTestWithRightValues() {
            Unit unit1 = UnitFactory.createUnit("CommanderUnit", "Unit1", 100);
            assertEquals(CommanderUnit.class, unit1.getClass());

            try {
                Unit unit2 = UnitFactory.createUnit("CommanderUnit", "Unit2", 100);
            } catch (Exception e) {
                fail("This test shouldn't give out an exception.");
            }
        }

        @Test
        @DisplayName("Exception handling test")
        public void unitCreationValueTest() {
            //Creates unit with negative health
            try {
                Unit unitError = UnitFactory.createUnit("CommanderUnit", "Unit1", -50);
                fail("Method did not throw IllegalArgumentException as expected");
            } catch (IllegalArgumentException ex) {
                assertEquals("A units health-stat, can't start out negative", ex.getMessage());
            }

            //Creates unit with no name
            try {
                Unit unitError = UnitFactory.createUnit("CommanderUnit", "", 100);
                fail("Method did not throw IllegalArgumentException as expected");
            } catch (IllegalArgumentException ex) {
                assertEquals("A units name can't be blank", ex.getMessage());
            }
        }
    }

    @Nested
    @DisplayName("Commander variable tests")
    public class TestsForUnitVariables {
        @Test
        @DisplayName("Get and set name test")
        public void unitNameTest() {
            Unit testUnit = UnitFactory.createUnit("CommanderUnit", "JackTheRipper", 50);
            assertEquals("JackTheRipper", testUnit.getName());

            Unit testUnit2 = UnitFactory.createUnit("CommanderUnit", "Name1", 50);
            testUnit2.setName("Name2");
            assertEquals("Name2", testUnit2.getName());

            Unit testUnit3 = UnitFactory.createUnit("CommanderUnit", "Lucas", 50);
            try {
                testUnit3.setName("");
                fail("Method did not throw IllegalArgumentException as expected");
            } catch (IllegalArgumentException ex) {
                assertEquals("A units name can't be blank", ex.getMessage());
            }
        }

        @Test
        @DisplayName("Attack test")
        public void commanderAttackTest() {
            Unit Commander1 = UnitFactory.createUnit("CommanderUnit", "Commander", 100);
            Unit Commander2 = UnitFactory.createUnit("CommanderUnit", "Commander", 100);

            Commander1.attack(Commander2, "DEFAULT");
            assertEquals(Commander2.getHealth(), 85);

            Commander1.attack(Commander2, "DEFAULT");
            assertEquals(Commander2.getHealth(), 74);
        }

        @Test
        @DisplayName("Attack test with terrain")
        public void commanderTerrainAttackTest() {
            Unit Commander1 = UnitFactory.createUnit("CommanderUnit", "Commander", 100);
            Unit Commander2 = UnitFactory.createUnit("CommanderUnit", "Commander", 100);

            Commander1.attack(Commander2, "PLAINS");
            assertEquals(83, Commander2.getHealth());

            Commander1.attack(Commander2, "PLAINS");
            assertEquals(70, Commander2.getHealth());

            Commander2.attack(Commander1, "FOREST");
            assertEquals(84, Commander1.getHealth());
        }
    }
}
