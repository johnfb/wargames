package edu.ntnu.idatt2001.johnfb.wargames.units.unitTypes;

import edu.ntnu.idatt2001.johnfb.wargames.objects.units.Unit;
import edu.ntnu.idatt2001.johnfb.wargames.objects.units.unitTypes.InfantryUnit;
import edu.ntnu.idatt2001.johnfb.wargames.util.UnitFactory;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Class is used to test the InfantryUnit-class
 * @author johnfb
 * @version 1.0
 */
public class InfantryUnitTest {
    @Nested
    @DisplayName("Infantry Constructor Tests")
    public class TestsForInfantryConstructor {
        @Test
        @DisplayName("Infantry creation test")
        public void unitCreationTestWithRightValues() {
            Unit unit1 = UnitFactory.createUnit("InfantryUnit", "Unit1", 100);
            assertEquals(InfantryUnit.class, unit1.getClass());

            try {
                Unit unit2 = UnitFactory.createUnit("InfantryUnit", "Unit2", 100);
            } catch (Exception e) {
                fail("This test shouldn't give out an exception.");
            }
        }

        @Test
        @DisplayName("Exception handling test")
        public void unitCreationValueTest() {
            //Creates unit with negative health
            try {
                Unit unitError = UnitFactory.createUnit("InfantryUnit", "Unit1", -50);
                fail("Method did not throw IllegalArgumentException as expected");
            } catch (IllegalArgumentException ex) {
                assertEquals("A units health-stat, can't start out negative", ex.getMessage());
            }

            //Creates unit with no name
            try {
                Unit unitError = UnitFactory.createUnit("InfantryUnit", "", 100);
                fail("Method did not throw IllegalArgumentException as expected");
            } catch (IllegalArgumentException ex) {
                assertEquals("A units name can't be blank", ex.getMessage());
            }
        }
    }

    @Nested
    @DisplayName("Infantry variable tests")
    public class TestsForUnitVariables {
        @Test
        @DisplayName("Get and set name test")
        public void unitNameTest() {
            Unit testUnit = UnitFactory.createUnit("InfantryUnit", "JackTheRipper", 50);
            assertEquals("JackTheRipper", testUnit.getName());

            Unit testUnit2 = UnitFactory.createUnit("InfantryUnit", "Name1", 50);
            testUnit2.setName("Name2");
            assertEquals("Name2", testUnit2.getName());

            Unit testUnit3 = UnitFactory.createUnit("InfantryUnit", "Lucas", 50);
            try {
                testUnit3.setName("");
                fail("Method did not throw IllegalArgumentException as expected");
            } catch (IllegalArgumentException ex) {
                assertEquals("A units name can't be blank", ex.getMessage());
            }
        }

        @Test
        @DisplayName("Attack test")
        public void infantryAttackTest() {
            Unit Infantry1 = UnitFactory.createUnit("InfantryUnit", "Infantry", 100);
            Unit Infantry2 = UnitFactory.createUnit("InfantryUnit", "Infantry", 100);

            Infantry1.attack(Infantry2, "DEFAULT");
            assertEquals(Infantry2.getHealth(), 94);
        }

        @Test
        @DisplayName("Attack test with terrain")
        public void infantryTerrainAttackTest() {
            Unit Infantry1 = UnitFactory.createUnit("InfantryUnit", "Infantry", 100);
            Unit Infantry2 = UnitFactory.createUnit("InfantryUnit", "Infantry", 100);

            Infantry1.attack(Infantry2, "FOREST");
            assertEquals(93, Infantry2.getHealth());

            Infantry1.attack(Infantry2, "HILL");
            assertEquals(87, Infantry2.getHealth());

            Infantry1.attack(Infantry2, "PLAINS");
            assertEquals(81, Infantry2.getHealth());
        }
    }
}
