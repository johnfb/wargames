package edu.ntnu.idatt2001.johnfb.wargames.units.unitTypes;

import edu.ntnu.idatt2001.johnfb.wargames.objects.units.Unit;
import edu.ntnu.idatt2001.johnfb.wargames.objects.units.unitTypes.CavalryUnit;
import edu.ntnu.idatt2001.johnfb.wargames.util.UnitFactory;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Class is used to test the CavalryUnit-class
 * @author johnfb
 * @version 1.0
 */
public class CavalryUnitTest {
    @Nested
    @DisplayName("Cavalry Constructor Tests")
    public class TestsForInfantryConstructor {
        @Test
        @DisplayName("Cavalry creation test")
        public void unitCreationTestWithRightValues() {
            Unit unit1 = UnitFactory.createUnit("CavalryUnit", "Unit1", 100);
            assertEquals(CavalryUnit.class, unit1.getClass());

            try {
                Unit unit2 = UnitFactory.createUnit("CavalryUnit", "Unit2", 100);
            } catch (Exception e) {
                fail("This test shouldn't give out an exception.");
            }
        }

        @Test
        @DisplayName("Exception handling test")
        public void unitCreationValueTest() {
            //Creates unit with negative health
            try {
                Unit unitError = UnitFactory.createUnit("CavalryUnit", "Unit1", -50);
                fail("Method did not throw IllegalArgumentException as expected");
            } catch (IllegalArgumentException ex) {
                assertEquals("A units health-stat, can't start out negative", ex.getMessage());
            }

            //Creates unit with no name
            try {
                Unit unitError = UnitFactory.createUnit("CavalryUnit", "", 100);
                fail("Method did not throw IllegalArgumentException as expected");
            } catch (IllegalArgumentException ex) {
                assertEquals("A units name can't be blank", ex.getMessage());
            }
        }
    }

    @Nested
    @DisplayName("Cavalry variable tests")
    public class TestsForUnitVariables {
        @Test
        @DisplayName("Get and set name test")
        public void unitNameTest() {
            Unit testUnit = UnitFactory.createUnit("CavalryUnit", "JackTheRipper", 50);
            assertEquals("JackTheRipper", testUnit.getName());

            Unit testUnit2 = UnitFactory.createUnit("CavalryUnit", "Name1", 50);
            testUnit2.setName("Name2");
            assertEquals("Name2", testUnit2.getName());

            Unit testUnit3 = UnitFactory.createUnit("CavalryUnit", "Lucas", 50);
            try {
                testUnit3.setName("");
                fail("Method did not throw IllegalArgumentException as expected");
            } catch (IllegalArgumentException ex) {
                assertEquals("A units name can't be blank", ex.getMessage());
            }
        }

        @Test
        @DisplayName("Attack test")
        public void cavalryAttackTest() {
            Unit Cavalry1 = UnitFactory.createUnit("CavalryUnit", "Cavalry", 100);
            Unit Cavalry2 = UnitFactory.createUnit("CavalryUnit", "Cavalry", 100);

            Cavalry1.attack(Cavalry2, "DEFAULT");
            assertEquals(Cavalry2.getHealth(), 87);

            Cavalry1.attack(Cavalry2, "DEFAULT");
            assertEquals(Cavalry2.getHealth(), 78);
        }

        @Test
        @DisplayName("Attack test with terrain")
        public void cavalryTerrainAttackTest() {
            Unit Cavalry1 = UnitFactory.createUnit("CavalryUnit", "Cavalry", 100);
            Unit Cavalry2 = UnitFactory.createUnit("CavalryUnit", "Cavalry", 100);

            Cavalry1.attack(Cavalry2, "PLAINS");
            assertEquals(85, Cavalry2.getHealth());

            Cavalry1.attack(Cavalry2, "PLAINS");
            assertEquals(74, Cavalry2.getHealth());

            Cavalry2.attack(Cavalry1, "FOREST");
            assertEquals(86, Cavalry1.getHealth());
        }
    }
}
