package edu.ntnu.idatt2001.johnfb.wargames.units.unitTypes;

import edu.ntnu.idatt2001.johnfb.wargames.objects.units.Unit;
import edu.ntnu.idatt2001.johnfb.wargames.objects.units.unitTypes.RangedUnit;
import edu.ntnu.idatt2001.johnfb.wargames.util.UnitFactory;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Class is used to test the RangedUnit-class
 * @author johnfb
 * @version 1.0
 */
public class RangedUnitTest {
    @Nested
    @DisplayName("Ranged Constructor Tests")
    public class TestsForInfantryConstructor {
        @Test
        @DisplayName("Ranged creation test")
        public void unitCreationTestWithRightValues() {
            Unit unit1 = UnitFactory.createUnit("RangedUnit", "Unit1", 100);
            assertEquals(RangedUnit.class, unit1.getClass());

            try {
                Unit unit2 = UnitFactory.createUnit("RangedUnit", "Unit2", 100);
            } catch (Exception e) {
                fail("This test shouldn't give out an exception.");
            }
        }

        @Test
        @DisplayName("Exception handling test")
        public void unitCreationValueTest() {
            //Creates unit with negative health
            try {
                Unit unitError = UnitFactory.createUnit("RangedUnit", "Unit1", -50);
                fail("Method did not throw IllegalArgumentException as expected");
            } catch (IllegalArgumentException ex) {
                assertEquals("A units health-stat, can't start out negative", ex.getMessage());
            }

            //Creates unit with no name
            try {
                Unit unitError = UnitFactory.createUnit("RangedUnit", "", 100);
                fail("Method did not throw IllegalArgumentException as expected");
            } catch (IllegalArgumentException ex) {
                assertEquals("A units name can't be blank", ex.getMessage());
            }
        }
    }

    @Nested
    @DisplayName("Ranged variable tests")
    public class TestsForUnitVariables {
        @Test
        @DisplayName("Get and set name test")
        public void unitNameTest() {
            Unit testUnit = UnitFactory.createUnit("RangedUnit", "JackTheRipper", 50);
            assertEquals("JackTheRipper", testUnit.getName());

            Unit testUnit2 = UnitFactory.createUnit("RangedUnit", "Name1", 50);
            testUnit2.setName("Name2");
            assertEquals("Name2", testUnit2.getName());

            Unit testUnit3 = UnitFactory.createUnit("RangedUnit", "Lucas", 50);
            try {
                testUnit3.setName("");
                fail("Method did not throw IllegalArgumentException as expected");
            } catch (IllegalArgumentException ex) {
                assertEquals("A units name can't be blank", ex.getMessage());
            }
        }

        @Test
        @DisplayName("Attack test")
        public void rangedAttackTest() {
            Unit Ranged1 = UnitFactory.createUnit("RangedUnit", "Ranged", 100);
            Unit Ranged2 = UnitFactory.createUnit("RangedUnit", "Ranged", 100);

            Ranged1.attack(Ranged2, "DEFAULT");
            assertEquals(Ranged2.getHealth(), 96);

            Ranged1.attack(Ranged2, "DEFAULT");
            assertEquals(Ranged2.getHealth(), 90);

            Ranged1.attack(Ranged2, "DEFAULT");
            assertEquals(Ranged2.getHealth(), 82);
        }

        @Test
        @DisplayName("Attack test with terrain")
        public void rangedTerrainAttackTest() {
            Unit Ranged1 = UnitFactory.createUnit("RangedUnit", "Ranged", 100);
            Unit Ranged2 = UnitFactory.createUnit("RangedUnit", "Ranged", 100);

            Ranged1.attack(Ranged2, "FOREST");
            assertEquals(98, Ranged2.getHealth());

            Ranged2.attack(Ranged1, "HILL");
            assertEquals(94, Ranged1.getHealth());
        }
    }
}
