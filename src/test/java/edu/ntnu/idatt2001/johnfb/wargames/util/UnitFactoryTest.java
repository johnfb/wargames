package edu.ntnu.idatt2001.johnfb.wargames.util;

import edu.ntnu.idatt2001.johnfb.wargames.objects.units.Unit;
import edu.ntnu.idatt2001.johnfb.wargames.objects.units.unitTypes.CavalryUnit;
import edu.ntnu.idatt2001.johnfb.wargames.objects.units.unitTypes.CommanderUnit;
import edu.ntnu.idatt2001.johnfb.wargames.objects.units.unitTypes.InfantryUnit;
import edu.ntnu.idatt2001.johnfb.wargames.objects.units.unitTypes.RangedUnit;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class used to test the UnitFactory
 * @author johnfb
 * @version 1.0
 */
public class UnitFactoryTest {
    @Nested
    @DisplayName("UnitFactory functionality tests")
    public class FunctionalityTests {
        @Test
        @DisplayName("Tests if an unit can be created")
        public void createUnitTest() {
            Unit unit1 = UnitFactory.createUnit("InfantryUnit", "Infantry", 50);
            assertInstanceOf(InfantryUnit.class, unit1);
            assertEquals(50, unit1.getHealth());
            assertEquals("Infantry", unit1.getName());

            Unit unit2 = UnitFactory.createUnit("RangedUnit", "Ranged", 50);
            assertInstanceOf(RangedUnit.class, unit2);

            Unit unit3 = UnitFactory.createUnit("CavalryUnit", "Cavalry", 50);
            assertInstanceOf(CavalryUnit.class, unit3);

            Unit unit4 = UnitFactory.createUnit("CommanderUnit", "Commander", 50);
            assertInstanceOf(CommanderUnit.class, unit4);
        }

        @Test
        @DisplayName("Tests if multiple units can be created")
        public void createUnitsTest() {
            ArrayList<Unit> units = UnitFactory.createUnits("InfantryUnit", "Infantry", 50, 10);
            assertEquals(10 , units.size());
            assertInstanceOf(InfantryUnit.class, units.get(4));
            assertEquals(50, units.get(4).getHealth());
            assertEquals("Infantry", units.get(4).getName());
        }
    }

    @Nested
    @DisplayName("UnitFactory exception-handling tests")
    public class ExceptionHandlingTests {
        @Test
        @DisplayName("Tests if you can create negative amount of units")
        public void createNegativeUnitsTest() {
            try {
                UnitFactory.createUnits("InfantryUnit", "Infantry", 50, -10);
            } catch (IllegalArgumentException e) {
                assertEquals("Can't create less than 0 units", e.getMessage());
            }
        }

        @Test
        @DisplayName("Tests if you can create a non existing unit-type")
        public void createNonExistingUnitTest() {
            try {
                UnitFactory.createUnit("ElephantUnit", "Infantry", 50);
            } catch (IllegalArgumentException e) {
                assertEquals("The unit type: 'ElephantUnit' does not exist.", e.getMessage());
            }
        }
    }
}
