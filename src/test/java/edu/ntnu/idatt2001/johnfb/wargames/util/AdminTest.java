package edu.ntnu.idatt2001.johnfb.wargames.util;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Class used to test the Admin class
 * @author johnfb
 * @version 1.0
 */
public class AdminTest {
    @Nested
    @DisplayName("Tests Admin-class functionality")
    public class FunctionalityTests {
        @Test
        @DisplayName("Tests if an army can be set")
        public void setArmyTest() {
            Admin.setHomeArmy("HomeArmy", 10, 10, 5, 5);
            assertEquals("HomeArmy", Admin.getHomeArmy().getName());
            assertEquals(10, Admin.getHomeArmy().getInfantryUnits().size());
            assertEquals(10, Admin.getHomeArmy().getRangedUnits().size());
            assertEquals(5, Admin.getHomeArmy().getCavalryUnits().size());
            assertEquals(5, Admin.getHomeArmy().getCommanderUnits().size());

            Admin.setAwayArmy("AwayArmy", 2, 2, 50, 50);
            assertEquals("AwayArmy", Admin.getAwayArmy().getName());
            assertEquals(2, Admin.getAwayArmy().getInfantryUnits().size());
            assertEquals(2, Admin.getAwayArmy().getRangedUnits().size());
            assertEquals(50, Admin.getAwayArmy().getCavalryUnits().size());
            assertEquals(50, Admin.getAwayArmy().getCommanderUnits().size());
        }

        @Test
        @DisplayName("Tests a battle can be set")
        public void setBattleTest() {
            Admin.setHomeArmy("HomeArmy", 10, 10, 5, 5);
            Admin.setAwayArmy("AwayArmy", 2, 2, 50, 50);

            try {
                Admin.startBattle("DEFAULT");
                assertTrue(true);
            } catch (Exception e) {
                fail(e.getMessage());
            }

            assertEquals("DEFAULT", Admin.getBattle().getTerrain().toString());
        }
    }

    @Nested
    @DisplayName("Tests Admin-class exception-handling")
    public class ExceptionHandlingTests {
        @Test
        @DisplayName("Tests if an invalid army can be set")
        public void setInvalidArmyTest() {
            try {
                Admin.setHomeArmy("HomeArmy", -10, 10, 5, 5);
            } catch (Exception e) {
                assertEquals("Can't create less than 0 units", e.getMessage());
            }

            try {
                Admin.setAwayArmy("", 10, 10, 5, 5);
            } catch (Exception e) {
                assertEquals("Name can't be blank", e.getMessage());
            }
        }
    }
}
