package edu.ntnu.idatt2001.johnfb.wargames.simulation;

import edu.ntnu.idatt2001.johnfb.wargames.objects.simulation.Army;
import edu.ntnu.idatt2001.johnfb.wargames.objects.simulation.Battle;
import edu.ntnu.idatt2001.johnfb.wargames.objects.units.*;
import edu.ntnu.idatt2001.johnfb.wargames.util.UnitFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Class is used to test the Battle-class.
 * @author johnfb
 * @version 1.0
 */
public class BattleTest {
    private Unit unit1;
    private Unit unit2;
    private Unit unit3;
    private Unit unit4;
    private Unit unit5;
    private Unit unit6;

    @BeforeEach
    public void initEach() {
        unit1 = UnitFactory.createUnit("InfantryUnit", "Unit1", 100);
        unit2 = UnitFactory.createUnit("InfantryUnit", "Unit2", 100);
        unit3 = UnitFactory.createUnit("InfantryUnit", "Unit3", 100);
        unit4 = UnitFactory.createUnit("InfantryUnit", "Unit4", 100);
        unit5 = UnitFactory.createUnit("RangedUnit", "Unit5", 75);
        unit6 = UnitFactory.createUnit("CavalryUnit", "Unit6", 75);
    }

    @Nested
    public class CreateBattleTests {
        /**
         * Tests the creation of a battle with two armies.
         * Tests what a battle can't start with empty armies.
         */
        @Test
        @DisplayName("Tests if a battle can be created")
        public void battleCreationTest() {
            Army army1 = new Army("army1");
            Army army2 = new Army("army2");

            //Tries with two armies that both have units.
            army2.add(unit2);
            army1.add(unit1);
            Battle battle2 = new Battle(army1, army2);
            assertEquals(battle2.getClass(), Battle.class);

            army1.add(unit2);
            Battle battle3 = new Battle(army2, army1);
            assertEquals(battle3.getClass(), Battle.class);
        }

        @Test
        @DisplayName("Tests battles army exception-handling")
        public void battleExceptionHandlingTest() {
            Army army1 = new Army("army1");
            Army army2 = new Army("army2");

            army1.add(unit1);

            //Tries to create a battle with an army with no units
            try {
                Battle battle1 = new Battle(army1, army2);
                fail("Method did not throw IllegalArgumentException as expected");
            } catch (IllegalArgumentException ex) {
                assertEquals(ex.getMessage(), "Both armies must at least have one unit");
            }

            try {
                Battle battle2 = new Battle(army2, army1);
                fail("Method did not throw IllegalArgumentException as expected");
            } catch (IllegalArgumentException ex) {
                assertEquals(ex.getMessage(), "Both armies must at least have one unit");
            }
        }
    }

    @Nested
    public class SimulateBattleTests {
        /**
         * Tests the simulation of a battle
         */
        @Test
        @DisplayName("Tests if a battle can be simulated")
        public void simulateTest() {
            Army army1 = new Army("army1");
            army1.add(unit1);
            army1.add(unit2);
            army1.add(unit3);
            army1.add(unit4);
            army1.add(unit5);

            Army army2 = new Army("army2");
            army2.add(unit6);

            Battle battle1 = new Battle(army1, army2);

            try {
                assertEquals(battle1.simulate(), army1);
            } catch (Exception e){
                fail("The weaker army won");
            }

            //Army2 shouldn't have any units left
            assertFalse(army2.hasUnits());
        }

        /**
         * Tests the simulation of a battle,
         * just more units
         */
        @Test
        @DisplayName("Tests more battle simulations")
        public void simulateMoreTest() {
            Army army1 = new Army("army1");
            army1.add(unit3);
            army1.add(unit4);
            army1.add(unit5);

            Army army2 = new Army("army2");
            army2.add(unit6);
            army2.add(unit1);
            army2.add(unit2);

            Battle battle1 = new Battle(army1, army2);

            try {
                assertEquals(battle1.simulate(), army2);
            } catch (Exception e){
                fail("'simulateTest' failed");
            }

            //Army1 shouldn't have any units left
            assertFalse(army1.hasUnits());
        }
    }
}
