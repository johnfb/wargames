package edu.ntnu.idatt2001.johnfb.wargames.objects.observers;

import java.util.ArrayList;

/**
 * Class is used to observe how each army is doing during a battle.
 * It stores the information of how many units each army has
 * for each step of the battle.
 *
 * @author johnfb
 * @version 1.0
 */
public class BattleObserver {
    private static ArrayList<Integer> homeArmyHistory = new ArrayList<Integer>();
    private static ArrayList<Integer> awayArmyHistory = new ArrayList<Integer>();

    /**
     * Basic constructor
     */
    public BattleObserver(){}

    /**
     * Method for starting a new observation.
     * Erases previous data.
     */
    public static void start() {
        homeArmyHistory.clear();
        awayArmyHistory.clear();
    }

    /**
     * Stores the amount of units left in each army
     * @param homeArmyUnits Number of units left in the home-army
     * @param awayArmyUnits Number of units left in the away-army
     */
    public static void update(int homeArmyUnits, int awayArmyUnits) {
        homeArmyHistory.add(homeArmyUnits);
        awayArmyHistory.add(awayArmyUnits);
    }

    /**
     * @return List of home-army's unit history
     */
    public static ArrayList<Integer> getHomeArmyHistory() {
        return homeArmyHistory;
    }

    /**
     * @return List of away-army's unit history
     */
    public static ArrayList<Integer> getAwayArmyHistory() {
        return awayArmyHistory;
    }

    /**
     * Method for getting and removing the next object of the home-army history list
     * @return Next value of the arraylist
     */
    public static int getNextHomeArmyHistory() {
        int num = homeArmyHistory.get(0);
        homeArmyHistory.remove(0);
        return num;
    }

    /**
     * Method for getting and removing the next object of the away-army history list
     * @return Next value of the arraylist
     */
    public static int getNextAwayArmyHistory() {
        int num = awayArmyHistory.get(0);
        awayArmyHistory.remove(0);
        return num;
    }
}
