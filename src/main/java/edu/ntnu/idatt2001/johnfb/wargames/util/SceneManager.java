package edu.ntnu.idatt2001.johnfb.wargames.util;

import edu.ntnu.idatt2001.johnfb.wargames.Main;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import java.io.IOException;

/**
 * Class used to change between scenes in the application
 * @version 1.0
 * @author johnfb and magnual
 */
public class SceneManager {
    private static Scene scene;

    /**
     * Basic constructor
     */
    public SceneManager() {}

    /**
     * Method for setting the view
     * @param viewFxml is the name of the fxml file
     * @throws IOException if the view couldn't be loaded
     */
    public static void setView(String viewFxml) throws IOException {
        FXMLLoader loader = getLoader(viewFxml);
        scene.setRoot(loader.load());
    }

    /**
     * Retrieves the FXML loader
     * @param fileName is the name of the FXML file
     * @return the FXMLLoader
     */
    public static FXMLLoader getLoader(String fileName) {
        String path = String.format("%s.fxml", fileName);
        System.out.println(path);
        return new FXMLLoader(Main.class.getResource(path));
    }

    /**
     * Sets the scenes
     * @param scene is the scene being set
     */
    public static void setScene(Scene scene) {
        SceneManager.scene = scene;
    }

    /**
     * Retrieves a scene
     * @return the scene
     */
    public static Scene getScene() {
        return scene;
    }
}
