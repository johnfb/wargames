package edu.ntnu.idatt2001.johnfb.wargames;

import edu.ntnu.idatt2001.johnfb.wargames.util.SceneManager;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;

public class Main extends Application {
    /**
     * Starts the application
     */
    @Override
    public void start(Stage stage) {
        try {
            FXMLLoader loader = SceneManager.getLoader("main");
            SceneManager.setScene(new Scene(loader.load()));
            SceneManager.setView("main");
            stage.setScene(SceneManager.getScene());
            stage.setTitle("Wargames");
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Main
     * @param args args
     */
    public static void main(String[] args) {
        launch(args);
    }
}
