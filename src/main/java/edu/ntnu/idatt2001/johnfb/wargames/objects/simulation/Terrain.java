package edu.ntnu.idatt2001.johnfb.wargames.objects.simulation;

/**
 * Terrain enum which is used in a battle
 */
public enum Terrain {
    DEFAULT,
    HILL,
    PLAINS,
    FOREST
}
