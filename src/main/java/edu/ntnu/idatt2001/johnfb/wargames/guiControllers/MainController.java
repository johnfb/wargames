package edu.ntnu.idatt2001.johnfb.wargames.guiControllers;

import edu.ntnu.idatt2001.johnfb.wargames.objects.simulation.Terrain;
import edu.ntnu.idatt2001.johnfb.wargames.util.Admin;
import edu.ntnu.idatt2001.johnfb.wargames.util.SceneManager;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import java.awt.*;
import java.io.IOException;

/**
 * Controller for the main-page
 * used to creating/loading armies and starting battles
 * @author johnfb
 * @version 1.0
 */
public class MainController {
    @FXML
    private TextField numberOfCavalryHome;
    @FXML
    private TextField numberOfInfantryHome;
    @FXML
    private TextField armyNameHome;
    @FXML
    private TextField numberOfRangedHome;
    @FXML
    private TextField numberOfCommanderHome;
    @FXML
    private ChoiceBox terrainBox;
    @FXML
    private TextField numberOfCommanderAway;
    @FXML
    private TextField numberOfRangedAway;
    @FXML
    private TextField numberOfInfantryAway;
    @FXML
    private TextField armyNameAway;
    @FXML
    private TextField numberOfCavalryAway;
    @FXML
    private Label errorLabel;
    @FXML
    private Label awayArmySource;
    @FXML
    private Label homeArmySource;

    //Local variables
    private int homeInfantries;
    private int homeRangers;
    private int homeCavalries;
    private int homeCommanders;
    private int awayInfantries;
    private int awayRangers;
    private int awayCavalries;
    private int awayCommanders;

    /**
     * Method running on launch
     * Adds terrain values
     * Fills in army information
     */
    @FXML
    public void initialize() {
        for (Terrain terrain : Terrain.values()) {
            terrainBox.getItems().add(terrain.toString());
        }
        terrainBox.setValue(terrainBox.getItems().get(0));
        fillInfo();
    }

    /**
     * Fills in army info if any armies are stored in the Admin-class
     */
    @FXML
    public void fillInfo() {
        if (Admin.getHomeArmy() != null) {
            armyNameHome.setText(Admin.getHomeArmy().getName());
            numberOfInfantryHome.setText(Admin.getHomeArmy().getInfantryUnits().size() + "");
            numberOfRangedHome.setText(Admin.getHomeArmy().getRangedUnits().size() + "");
            numberOfCavalryHome.setText(Admin.getHomeArmy().getCavalryUnits().size() + "");
            numberOfCommanderHome.setText(Admin.getHomeArmy().getCommanderUnits().size() + "");
        }

        if (Admin.getAwayArmy() != null) {
            armyNameAway.setText(Admin.getAwayArmy().getName());
            numberOfInfantryAway.setText(Admin.getAwayArmy().getInfantryUnits().size() + "");
            numberOfRangedAway.setText(Admin.getAwayArmy().getRangedUnits().size() + "");
            numberOfCavalryAway.setText(Admin.getAwayArmy().getCavalryUnits().size() + "");
            numberOfCommanderAway.setText(Admin.getAwayArmy().getCommanderUnits().size() + "");
        }
    }

    /**
     * Checks if all the numbers have a valid input
     * Gives out an error message if any inputs are invalid
     */
    @FXML
    private void checkNumbers() {
        try {
            homeInfantries = Integer.parseInt(numberOfInfantryHome.getText());
            homeRangers = Integer.parseInt(numberOfRangedHome.getText());
            homeCavalries = Integer.parseInt(numberOfCavalryHome.getText());
            homeCommanders = Integer.parseInt(numberOfCommanderHome.getText());
            awayInfantries = Integer.parseInt(numberOfInfantryAway.getText());
            awayRangers = Integer.parseInt(numberOfRangedAway.getText());
            awayCavalries = Integer.parseInt(numberOfCavalryAway.getText());
            awayCommanders = Integer.parseInt(numberOfCommanderAway.getText());
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Must enter a valid number");
        }
    }

    /**
     * Increase the number of home infantry units
     */
    @FXML
    public void upInfantryHome() {
        try {
            homeInfantries = Integer.parseInt(numberOfInfantryHome.getText());
            homeInfantries++;
        } catch (Exception e) {
            homeInfantries = 1;
        }
        numberOfInfantryHome.setText(homeInfantries + "");
    }

    /**
     * Increase the number of home ranged units
     */
    @FXML
    public void upRangedHome() {
        try {
            homeRangers = Integer.parseInt(numberOfRangedHome.getText());
            homeRangers++;
        } catch (Exception e) {
            homeRangers = 1;
        }
        numberOfRangedHome.setText(homeRangers + "");
    }

    /**
     * Increase the number of home cavalry units
     */
    @FXML
    public void upCavalryHome() {
        try {
            homeCavalries = Integer.parseInt(numberOfCavalryHome.getText());
            homeCavalries++;
        } catch (Exception e) {
            homeCavalries = 1;
        }
        numberOfCavalryHome.setText(homeCavalries + "");
    }

    /**
     * Decrease the number of home infantry units
     */
    @FXML
    public void downInfantryHome() {
        try {
            homeInfantries = Integer.parseInt(numberOfInfantryHome.getText());
            if (homeInfantries < 1) {
                homeInfantries = 0;
            }
            else {
                homeInfantries--;
            }
        } catch (Exception e) {
            homeInfantries = 0;
        }
        numberOfInfantryHome.setText(homeInfantries + "");
    }

    /**
     * Decrease the number of home ranged units
     */
    @FXML
    public void downRangedHome() {
        try {
            homeRangers = Integer.parseInt(numberOfRangedHome.getText());
            if (homeRangers < 1) {
                homeRangers = 0;
            }
            else {
                homeRangers--;
            }
        } catch (Exception e) {
            homeRangers = 0;
        }
        numberOfRangedHome.setText(homeRangers + "");
    }

    /**
     * Decrease the number of home commander units
     */
    @FXML
    public void downCommanderHome() {
        try {
            homeCommanders = Integer.parseInt(numberOfCommanderHome.getText());
            if (homeCommanders < 1) {
                homeCommanders = 0;
            }
            else {
                homeCommanders--;
            }
        } catch (Exception e) {
            homeCommanders = 0;
        }
        numberOfCommanderHome.setText(homeCommanders + "");
    }

    /**
     * Decrease the number of home cavalry units
     */
    @FXML
    public void downCavalryHome() {
        try {
            homeCavalries = Integer.parseInt(numberOfCavalryHome.getText());
            if (homeCavalries < 1) {
                homeCavalries = 0;
            }
            else {
                homeCavalries--;
            }
        } catch (Exception e) {
            homeCavalries = 0;
        }
        numberOfCavalryHome.setText(homeCavalries + "");
    }

    /**
     * Increase the number of home commander units
     */
    @FXML
    public void upCommanderHome() {
        try {
            homeCommanders = Integer.parseInt(numberOfCommanderHome.getText());
            homeCommanders++;
        } catch (Exception e) {
            homeCommanders = 1;
        }
        numberOfCommanderHome.setText(homeCommanders + "");
    }

    /**
     * Stores both armies and opens the advancedView-page
     */
    @FXML
    public void advancedView() {
        try {
            checkNumbers();
            Admin.setHomeArmy(armyNameHome.getText(), homeInfantries, homeRangers, homeCavalries, homeCommanders);
            Admin.setAwayArmy(armyNameAway.getText(), awayInfantries, awayRangers, awayCavalries, awayCommanders);
            SceneManager.setView("advancedView");
        } catch (Exception e) {
            errorLabel.setText(e.getMessage());
        }
    }

    /**
     * Registers two new armies from all the inputs,
     * creates a new battle between both armies
     */
    @FXML
    public void startBattle() {
        try {
            checkNumbers();
            if (homeInfantries < 0 || homeRangers < 0 || homeCavalries < 0 || homeCommanders < 0 ||
                    awayInfantries < 0 || awayRangers < 0 || awayCavalries < 0 || awayCommanders < 0) {
                throw new IllegalArgumentException("Can't enter a negative number");
            }
            Admin.setHomeArmy(armyNameHome.getText(), homeInfantries, homeRangers, homeCavalries, homeCommanders);
            Admin.setAwayArmy(armyNameAway.getText(), awayInfantries, awayRangers, awayCavalries, awayCommanders);
            Admin.startBattle(terrainBox.getValue().toString());
            SceneManager.setView("battle");
        } catch (Exception e) {
            errorLabel.setText(e.getMessage());
        }
    }

    /**
     * Clears every home-army variable
     */
    @FXML
    public void clearArmyHome() {
        armyNameHome.setText("");
        numberOfInfantryHome.setText("0");
        numberOfRangedHome.setText("0");
        numberOfCavalryHome.setText("0");
        numberOfCommanderHome.setText("0");
    }

    /**
     * Loads a home army from file
     */
    @FXML
    public void loadArmyHome() {
        FileDialog dialog = new FileDialog((Frame)null, "Select File to Open");
        dialog.setMode(FileDialog.LOAD);
        dialog.setVisible(true);
        try {
            Admin.loadHomeArmy(dialog.getFiles()[0]);
            homeArmySource.setText(dialog.getFiles()[0].getPath());
            armyNameHome.setText(Admin.getHomeArmy().getName());
            numberOfInfantryHome.setText(Admin.getHomeArmy().getInfantryUnits().size() + "");
            numberOfRangedHome.setText(Admin.getHomeArmy().getRangedUnits().size() + "");
            numberOfCavalryHome.setText(Admin.getHomeArmy().getCavalryUnits().size() + "");
            numberOfCommanderHome.setText(Admin.getHomeArmy().getCommanderUnits().size() + "");
        } catch (IOException e) {
            errorLabel.setText(e.getMessage());
        }
    }

    /**
     * Saves the Home army to a local file
     */
    @FXML
    public void saveArmyHome() {
        try {
            checkNumbers();

            FileDialog fileDialog = new FileDialog(new Frame(), "Save", FileDialog.SAVE);
            fileDialog.setFilenameFilter((dir, name) -> name.endsWith(".csv"));
            fileDialog.setVisible(true);
            System.out.println("File: " + fileDialog.getFile());
            Admin.saveHomeArmy(fileDialog.getFiles()[0], armyNameHome.getText(), homeInfantries, homeRangers, homeCavalries, homeCommanders);
            homeArmySource.setText(fileDialog.getFiles()[0].getPath());
        } catch (Exception e) {
            errorLabel.setText(e.getMessage());
        }
    }

    /**
     * Increase the number of away ranged units
     */
    @FXML
    public void upRangedAway() {
        try {
            awayRangers = Integer.parseInt(numberOfRangedAway.getText());
            awayRangers++;
        } catch (Exception e) {
            awayRangers = 1;
        }
        numberOfRangedAway.setText(awayRangers + "");
    }

    /**
     * Saves the Away army to a local file
     */
    @FXML
    public void saveArmyAway() {
        try {
            checkNumbers();

            FileDialog fileDialog = new FileDialog(new Frame(), "Save", FileDialog.SAVE);
            fileDialog.setFilenameFilter((dir, name) -> name.endsWith(".csv"));
            fileDialog.setVisible(true);
            System.out.println("File: " + fileDialog.getFile());
            Admin.saveAwayArmy(fileDialog.getFiles()[0], armyNameAway.getText(), awayInfantries, awayRangers, awayCavalries, awayCommanders);
            awayArmySource.setText(fileDialog.getFiles()[0].getPath());
        } catch (Exception e) {
            errorLabel.setText(e.getMessage());
        }
    }

    /**
     * Increase the number of away infantry units
     */
    @FXML
    public void upInfantryAway() {
        try {
            awayInfantries = Integer.parseInt(numberOfInfantryAway.getText());
            awayInfantries++;
        } catch (Exception e) {
            awayInfantries = 1;
        }
        numberOfInfantryAway.setText(awayInfantries + "");
    }

    /**
     * Increase the number of away cavalry units
     */
    @FXML
    public void upCavalryAway() {
        try {
            awayCavalries = Integer.parseInt(numberOfCavalryAway.getText());
            awayCavalries++;
        } catch (Exception e) {
            awayCavalries = 1;
        }
        numberOfCavalryAway.setText(awayCavalries + "");
    }

    /**
     * Decrease the number of away ranged units
     */
    @FXML
    public void downRangedAway() {
        try {
            awayRangers = Integer.parseInt(numberOfRangedAway.getText());
            if (awayRangers < 1) {
                awayRangers = 0;
            }
            else {
                awayRangers--;
            }
        } catch (Exception e) {
            awayRangers = 0;
        }
        numberOfRangedAway.setText(awayRangers + "");
    }

    /**
     * Decrease the number of away cavalry units
     */
    @FXML
    public void downCavalryAway() {
        try {
            awayCavalries = Integer.parseInt(numberOfCavalryAway.getText());
            if (awayCavalries < 1) {
                awayCavalries = 0;
            }
            else {
                awayCavalries--;
            }
        } catch (Exception e) {
            awayCavalries = 0;
        }
        numberOfCavalryAway.setText(awayCavalries + "");
    }

    /**
     * Increase the number of away commander units
     */
    @FXML
    public void upCommanderAway() {
        try {
            awayCommanders = Integer.parseInt(numberOfCommanderAway.getText());
            awayCommanders++;
        } catch (Exception e) {
            awayCommanders = 1;
        }
        numberOfCommanderAway.setText(awayCommanders + "");
    }

    /**
     * Loads an away army from file
     */
    @FXML
    public void loadArmyAway() {
        FileDialog dialog = new FileDialog((Frame)null, "Select File to Open");
        dialog.setMode(FileDialog.LOAD);
        dialog.setVisible(true);
        try {
            Admin.loadAwayArmy(dialog.getFiles()[0]);
            awayArmySource.setText(dialog.getFiles()[0].getPath());
            armyNameAway.setText(Admin.getAwayArmy().getName());
            numberOfInfantryAway.setText(Admin.getAwayArmy().getInfantryUnits().size() + "");
            numberOfRangedAway.setText(Admin.getAwayArmy().getRangedUnits().size() + "");
            numberOfCavalryAway.setText(Admin.getAwayArmy().getCavalryUnits().size() + "");
            numberOfCommanderAway.setText(Admin.getAwayArmy().getCommanderUnits().size() + "");
        } catch (IOException e) {
            errorLabel.setText(e.getMessage());
        }
    }

    /**
     * Clear all away army inputs
     */
    @FXML
    public void clearArmyAway() {
        armyNameAway.setText("");
        numberOfInfantryAway.setText("0");
        numberOfRangedAway.setText("0");
        numberOfCavalryAway.setText("0");
        numberOfCommanderAway.setText("0");
    }

    /**
     * Decrease the number of away infantry units
     */
    @FXML
    public void downInfantryAway() {
        try {
            awayInfantries = Integer.parseInt(numberOfInfantryAway.getText());
            if (awayInfantries < 1) {
                awayInfantries = 0;
            }
            else {
                awayInfantries--;
            }
        } catch (Exception e) {
            awayInfantries = 0;
        }
        numberOfInfantryAway.setText(awayInfantries + "");
    }

    /**
     * Decrease the number of away commander units
     */
    @FXML
    public void downCommanderAway() {
        try {
            awayCommanders = Integer.parseInt(numberOfCommanderAway.getText());
            if (awayCommanders < 1) {
                awayCommanders = 0;
            }
            else {
                awayCommanders--;
            }
        } catch (Exception e) {
            awayCommanders = 0;
        }
        numberOfCommanderAway.setText(awayCommanders + "");
    }
}
