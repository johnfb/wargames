package edu.ntnu.idatt2001.johnfb.wargames.objects.units.unitTypes;

import edu.ntnu.idatt2001.johnfb.wargames.objects.units.Unit;

/**
 * RangedUnit-class for creating a new unit of the type ranged.
 *
 * @author johnfb
 * @version 1.0
 */
public class RangedUnit extends Unit {
    private int numberOfTimesAttacked;

    /**
     * Constructor for the RangedUnit-class.
     * Attack is locked at 15, and armor is locked at 8.
     * @param name the name of the infantryUnit
     * @param health the health of the infantryUnit
     */
    public RangedUnit(String name, int health) throws IllegalArgumentException {
        super(name, health, 15, 8);
        numberOfTimesAttacked = 0;
    }

    /**
     * Gets small bonus if it's in a hill-terrain, and looses some bonus when it's in a forest-terrain.
     * @return RangedUnits attack-bonus
     */
    @Override
    public int getAttackBonus(String terrain) {
        if (terrain.equalsIgnoreCase("HILL")) {
            return 5;
        } else if (terrain.equalsIgnoreCase("FOREST")) {
            return 1;
        }
        return 3;
    }

    /**
     * The resist-bonus depends on how many times the unit has been attacked,
     * number of times attacked gets counted, and uses that value to decide the resist-bonus.
     * @return RangedUnits resist-bonus
     */
    @Override
    public int getResistBonus(String terrain) {
        numberOfTimesAttacked++;
        if (numberOfTimesAttacked == 1) {
            return 6;
        } else if (numberOfTimesAttacked == 2) {
            return 4;
        }
        return 2;
    }
}

