package edu.ntnu.idatt2001.johnfb.wargames.util;
import edu.ntnu.idatt2001.johnfb.wargames.objects.units.*;
import edu.ntnu.idatt2001.johnfb.wargames.objects.units.unitTypes.CavalryUnit;
import edu.ntnu.idatt2001.johnfb.wargames.objects.units.unitTypes.CommanderUnit;
import edu.ntnu.idatt2001.johnfb.wargames.objects.units.unitTypes.InfantryUnit;
import edu.ntnu.idatt2001.johnfb.wargames.objects.units.unitTypes.RangedUnit;

import java.util.ArrayList;

/**
 * Factory to create Units.
 * Can create one or multiple units.
 *
 * @author johnfb
 * @version 1.0
 * @since 14.04.22
 */
public class UnitFactory {
    private static final String infantryId = "InfantryUnit";
    private static final String rangedId = "RangedUnit";
    private static final String cavalryId = "CavalryUnit";
    private static final String commanderId = "CommanderUnit";

    public UnitFactory(){}

    /**
     * Creates a unit of any given type
     * @param type Unit type
     * @param name Unit name
     * @param health Unit health
     * @return A unit created with the given values
     * @throws IllegalArgumentException if any values are invalid
     */
    public static Unit createUnit(String type, String name, int health) throws IllegalArgumentException {
        if (type.isBlank())
            return null;

        switch (type) {
            case infantryId:
                return new InfantryUnit(name, health);
            case rangedId:
                return new RangedUnit(name, health);
            case cavalryId:
                return new CavalryUnit(name, health);
            case commanderId:
                return new CommanderUnit(name, health);
            default:
                throw new IllegalArgumentException("The unit type: " + "'" + type + "' does not exist.");
        }
    }

    /**
     * Creates a list of units of any given type
     * @param type Unit type
     * @param name Unit name
     * @param health Unit health
     * @param size Number og units to create
     * @return A unit created with the given values
     * @throws IllegalArgumentException if any values are invalid
     */
    public static ArrayList<Unit> createUnits(String type, String name, int health, int size) throws IllegalArgumentException {
        if (size < 0) {
            throw new IllegalArgumentException("Can't create less than 0 units");
        }

        ArrayList<Unit> units = new ArrayList<>();

        for (int i = 0; i < size; i++) {
            units.add(createUnit(type, name, health));
        }

        return units;
    }
}
