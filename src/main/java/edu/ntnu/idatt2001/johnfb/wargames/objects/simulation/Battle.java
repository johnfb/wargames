package edu.ntnu.idatt2001.johnfb.wargames.objects.simulation;

import edu.ntnu.idatt2001.johnfb.wargames.objects.observers.BattleObserver;
import edu.ntnu.idatt2001.johnfb.wargames.objects.units.Unit;

/**
 * Class for simulating a battle
 * between two armies.
 * @author johnfb
 * @version 1.0
 */
public class Battle {
    private Army armyOne;
    private Army armyTwo;
    private Terrain terrain;

    /**
     * Basic constructor without terrain
     * @param armyOne Army to fight
     * @param armyTwo Other army to fight
     */
    public Battle(Army armyOne, Army armyTwo) throws IllegalArgumentException {
        if (!armyOne.hasUnits() || !armyTwo.hasUnits()) {
            throw new IllegalArgumentException("Both armies must at least have one unit");
        }
        this.armyOne = armyOne;
        this.armyTwo = armyTwo;
        terrain = Terrain.DEFAULT;
    }

    /**
     * Basic constructor with terrain
     * @param armyOne Army to fight
     * @param armyTwo Other army to fight
     * @param terrain Terrain type
     */
    public Battle(Army armyOne, Army armyTwo, String terrain) throws IllegalArgumentException {
        if (!armyOne.hasUnits() || !armyTwo.hasUnits()) {
            throw new IllegalArgumentException("Both armies must at least have one unit");
        }
        this.terrain = Terrain.valueOf(terrain);
        this.armyOne = armyOne;
        this.armyTwo = armyTwo;
    }

    /**
     * Random unit from each army attacks one another,
     * until there are no alive unites left.
     * @return the army who won the battle.
     */
    public Army simulate() {
        Army victoriousArmy;
        BattleObserver.start();

        while (armyOne.hasUnits() && armyTwo.hasUnits()) { //Repeats until one army has no units left
            Unit unit1 = armyOne.getRandom();
            Unit unit2 = armyTwo.getRandom();

            unit1.attack(unit2, terrain.toString());
            //Eliminate the unit if it has no health left
            if (unit2.getHealth() <= 0) {
                armyTwo.remove(unit2);
                if (!armyTwo.hasUnits()) {
                    break;
                }
            }

            unit2.attack(unit1, terrain.toString());
            //Eliminate the unit if it has no health left
            if (unit1.getHealth() <= 0) {
                armyOne.remove(unit1);
            }

            BattleObserver.update(armyOne.getAllUnits().size(), armyTwo.getAllUnits().size());
        }

        BattleObserver.update(armyOne.getAllUnits().size(), armyTwo.getAllUnits().size());
        if (armyOne.hasUnits()) {
            victoriousArmy = armyOne;
        } else {
            victoriousArmy = armyTwo;
        }

        return victoriousArmy;
    }

    /**
     * @return Terrain
     */
    public Terrain getTerrain() {
        return terrain;
    }

    @Override
    public String toString() {
        return "Army1:\n" + armyOne + "\n" +
                "Army2:\n" + armyTwo;
    }
}
