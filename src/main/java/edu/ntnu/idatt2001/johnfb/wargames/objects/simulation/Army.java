package edu.ntnu.idatt2001.johnfb.wargames.objects.simulation;

import edu.ntnu.idatt2001.johnfb.wargames.objects.units.Unit;
import edu.ntnu.idatt2001.johnfb.wargames.util.UnitFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Class for creating an army,
 * which stores a list of units
 * @author johnfb
 * @version 1.0
 * @since 03.04.2022
 */
public class Army {
    private String name;
    private List<Unit> units = new ArrayList<>();

    /**
     * Basic constructor
     * @param name Army name
     */
    public Army(String name) throws IllegalArgumentException {
        if (name.isBlank()) {
            throw new IllegalArgumentException("Name can't be blank");
        }
        this.name = name;
    }

    /**
     * Constructor with a name and a list of units as input
     * @param name Army name
     * @param units A list of units
     */
    public Army(String name, List<Unit> units) throws IllegalArgumentException {
        if (name.isBlank()) {
            throw new IllegalArgumentException("Name can't be blank");
        }
        this.name = name;
        this.units = units;
    }

    /**
     * Constructor for deep-copying an army
     * @param army Army to copy
     */
    public Army(Army army) {
        this.name = army.getName();
        army.getAllUnits().forEach(Unit ->
                this.units.add(UnitFactory.createUnit(Unit.getClass().getSimpleName(), Unit.getName(), Unit.getHealth())));
    }

    public String getName() {
        return name;
    }

    /**
     * Adds a unit to the army
     * @param unit Unit to add
     */
    public void add(Unit unit) {
        this.units.add(unit);
    }

    /**
     * Adds a list of units to the army
     * @param units Units to add
     */
    public void addAll(List<Unit> units) {
        this.units.addAll(units);
    }

    /**
     * Removes a given unit from the army
     * @param unit Unit to remove
     */
    public void remove(Unit unit) throws IllegalArgumentException {
        if (!units.contains(unit)) {
            throw new IllegalArgumentException("The unit is not in this army");
        }
        this.units.remove(unit);
    }

    /**
     * Checks if the army is empty or not
     * @return True if army has units
     */
    public boolean hasUnits() {
        return !units.isEmpty();
    }

    /**
     * Get all units from the army
     * @return List of all units
     */
    public List<Unit> getAllUnits() {
        return units;
    }

    /**
     * Get a random unit from the army
     * @return A random unit
     */
    public Unit getRandom() {
        if (units.size() == 0) {
            return null;
        }

        Random random = new Random();
        int i = random.nextInt(units.size());
        return units.get(i);
    }

    /**
     * Finds all the infantry-units in the current army
     * @return List with all infantry-units
     */
    public ArrayList<Unit> getInfantryUnits() {
        ArrayList<Unit> newList;
        newList = units.stream().filter(unit -> unit.getClass().getSimpleName()
                .equals("InfantryUnit")).collect(Collectors.toCollection(ArrayList::new));
        return newList;
    }

    /**
     * Finds all the cavalry-units in the current army
     * @return List with all cavalry-units
     */
    public ArrayList<Unit> getCavalryUnits() {
        ArrayList<Unit> newList;
        newList = units.stream().filter(unit -> unit.getClass().getSimpleName()
                .equals("CavalryUnit")).collect(Collectors.toCollection(ArrayList::new));
        return newList;
    }

    /**
     * Finds all the ranged-units in the current army
     * @return List with all ranged-units
     */
    public ArrayList<Unit> getRangedUnits() {
        ArrayList<Unit> newList;
        newList = units.stream().filter(unit -> unit.getClass().getSimpleName()
                .equals("RangedUnit")).collect(Collectors.toCollection(ArrayList::new));
        return newList;
    }

    /**
     * Finds all the commander-units in the current army
     * @return List with all commander-units
     */
    public ArrayList<Unit> getCommanderUnits() {
        ArrayList<Unit> newList;
        newList = units.stream().filter(unit -> unit.getClass().getSimpleName()
                .equals("CommanderUnit")).collect(Collectors.toCollection(ArrayList::new));
        return newList;
    }

    /**
     * Method for changing the army name
     * @param name New army name
     * @throws IllegalArgumentException Gives error if name is blank
     */
    public void setName(String name) throws IllegalArgumentException {
        if (name.isBlank()) {
            throw new IllegalArgumentException("Name can't be empty");
        }
        this.name = name;
    }

    /**
     * Gives a list of all unit-toStrings
     * @return A list of Unit-ToStrings
     */
    public ArrayList<String> getAllUnitStrings() {
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < units.size(); i++) {
            list.add(i + ":\n" + units.get(i).toString());
        }
        return list;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Army - ").append(this.getName()).append("\n");
        for (Unit unit : units) {
            sb.append(unit).append("\n");
        }
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Army army = (Army) o;
        return Objects.equals(name, army.name) && Objects.equals(units, army.units);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, units);
    }
}
