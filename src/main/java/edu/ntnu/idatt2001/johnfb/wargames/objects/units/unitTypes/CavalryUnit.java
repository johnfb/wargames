package edu.ntnu.idatt2001.johnfb.wargames.objects.units.unitTypes;

import edu.ntnu.idatt2001.johnfb.wargames.objects.simulation.Terrain;
import edu.ntnu.idatt2001.johnfb.wargames.objects.units.Unit;

/**
 * CavalryUnit-class for creating a new unit of the type Cavalry.
 *
 * @author johnfb
 * @version 1.0
 */
public class CavalryUnit extends Unit {
    private boolean hasAttacked; //Checks if the unit has attacked someone.

    /**
     * Constructor for the CavalryUnit-class,
     * without locked attack- and armor-stats.
     * @param name the name of the CavalryUnit
     * @param health the health of the CavalryUnit
     * @param attack the attack of the CavalryUnit
     * @param armor the armor of the CavalryUnit
     */
    public CavalryUnit(String name, int health, int attack, int armor) throws IllegalArgumentException {
        super(name, health, attack, armor);
        hasAttacked = false;
    }

    /**
     * Constructor for the CavalryUnit-class.
     * Attack is locked at 20, and armor is locked at 12.
     * @param name the name of the CavalryUnit
     * @param health the health of the CavalryUnit
     */
    public CavalryUnit(String name, int health) {
        super(name, health, 20, 12);
        hasAttacked = false;
    }

    /**
     * CavalryUnits attack-bonus is greater on the first attack,
     * therefore it uses the hasAttacked boolean to check if it's the units first attack.
     * It also gets a bonus if the terrain is plains.
     * @return CavalryUnit's attack-bonus
     */
    @Override
    public int getAttackBonus(String terrain) {
        if (!hasAttacked) {
            hasAttacked = true;
            if (terrain.equalsIgnoreCase("PLAINS")) {
                return 8; //First attack deals greater damage.
            }
            return 6; //First attack deals greater damage.
        }

        if (terrain.equalsIgnoreCase("PLAINS")) {
            return 4;
        }
        return 2;
    }

    /**
     * The unit gets no bonus if it's in a forest-terrain.
     * @return CavalryUnits resist-bonus
     */
    @Override
    public int getResistBonus(String terrain) {
        if (terrain.equalsIgnoreCase("FOREST")) {
            return 0;
        }
        return 1;
    }
}
