package edu.ntnu.idatt2001.johnfb.wargames.util;

import edu.ntnu.idatt2001.johnfb.wargames.objects.simulation.Army;
import edu.ntnu.idatt2001.johnfb.wargames.objects.units.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * Class for managing files
 * Used to write and read armies from .csv files
 * @author johnfb
 * @version 1.0
 * @since 02.04.2022
 */
public class FileManager {
    public FileManager() {}

    /**
     * Method for writing an army onto a .csv file
     * @param army The army to write onto the file
     * @param fileName The name of the file
     * @throws IOException Throws if the program couldn't create a new file
     */
    public static void writeArmy(Army army, String fileName) throws IOException {
        if (fileName.isBlank()) {
            throw new IOException("You must enter a filename.");
        }

        if (!fileName.endsWith(".csv")) {
            fileName = fileName + ".csv";
        }

        try {
            FileWriter writer = new FileWriter(fileName);
            writer.append(army.getName()).append("\n");
            army.getAllUnits().forEach(Unit ->
                    {
                        try {
                            writer.append(Unit.getClass().getSimpleName()).append(",");
                            writer.append(Unit.getName()).append(",");
                            writer.append(String.valueOf(Unit.getHealth())).append("\n");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
            );
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
            throw new IOException("Was not able to create file");
        }
    }

    /**
     * Method for writing an army onto a .csv file
     * @param army The army to write onto the file
     * @param file The file to be written onto
     * @throws IOException Throws if the program couldn't create a new file
     */
    public static void writeArmyToFile(Army army, File file) throws IOException {
        if (file.getName().isBlank()) {
            throw new IOException("You must enter a filename.");
        }

        if (!file.getName().endsWith(".csv")) {
           throw new IOException("You must create a .csv file.");
        }

        try {
            FileWriter writer = new FileWriter(file.getPath());
            writer.append(army.getName()).append("\n");
            army.getAllUnits().forEach(Unit ->
                    {
                        try {
                            writer.append(Unit.getClass().getSimpleName()).append(",");
                            writer.append(Unit.getName()).append(",");
                            writer.append(String.valueOf(Unit.getHealth())).append("\n");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
            );
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
            throw new IOException("Was not able to create file");
        }
    }

    /**
     * Method for creating an army from a given .csv-file
     * @param file File to read
     * @return Army created from file
     * @throws IOException Error if there's something wrong with the file
     */
    public static Army readArmy(File file) throws IOException {
        if (!file.getName().endsWith(".csv")) {
            throw new IOException("Only .csv files are supported.");
        }
        Army army;
        try (Scanner scanner = new Scanner(file)){
            army = new Army(scanner.nextLine());
            while (scanner.hasNext()) {
                String currentLine = scanner.nextLine();
                String[] values = currentLine.split(",");

                if (values.length != 3) {
                    throw new IOException("File-data is invalid. Make sure the file is correctly formatted.");
                }

                String unitType = values[0];
                String unitName = values[1];
                int unitHealth;
                try {
                    unitHealth = Integer.parseInt(values[2]);
                } catch (NumberFormatException e) {
                    throw new IOException("The health value must be an integer.");
                }

                try {
                    Unit unit = UnitFactory.createUnit(unitType, unitName, unitHealth);
                    army.add(unit);
                } catch (IllegalArgumentException e) {
                    throw new IOException(e.getMessage());
                }
            }
        }

        return army;
    }
}
