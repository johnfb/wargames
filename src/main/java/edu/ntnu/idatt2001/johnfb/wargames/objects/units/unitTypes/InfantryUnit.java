package edu.ntnu.idatt2001.johnfb.wargames.objects.units.unitTypes;

import edu.ntnu.idatt2001.johnfb.wargames.objects.units.Unit;

/**
 * InfantryUnit-class for creating a new unit of the type Infantry.
 *
 * @author johnfb
 * @version 1.0
 */
public class InfantryUnit extends Unit {
    /**
     * Constructor for the InfantryUnit-class.
     * Attack is locked at 15, and armor is locked at 10.
     * @param name the name of the infantryUnit
     * @param health the health of the infantryUnit
     */
    public InfantryUnit(String name, int health) throws IllegalArgumentException {
        super(name, health, 15, 10);
    }

    /**
     * Gets a bonus when the Terrain is set to FOREST
     * @return InfantryUnits attack-bonus
     */
    @Override
    public int getAttackBonus(String terrain) {
        if (terrain.equalsIgnoreCase("FOREST")) {
            return 4;
        }
        return 2;
    }

    /**
     * Gets a bonus when the Terrain is set to FOREST
     * @return InfantryUnits resist-bonus
     */
    @Override
    public int getResistBonus(String terrain) {
        if (terrain.equalsIgnoreCase("FOREST")) {
            return 2;
        }
        return 1;
    }
}
