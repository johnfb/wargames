package edu.ntnu.idatt2001.johnfb.wargames;

/**
 * Class used to run the entire application
 */
public class App {
    public static void main(String[] args) {
        Main.main(args);
    }
}
