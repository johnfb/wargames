package edu.ntnu.idatt2001.johnfb.wargames.util;

import edu.ntnu.idatt2001.johnfb.wargames.objects.simulation.Army;
import edu.ntnu.idatt2001.johnfb.wargames.objects.simulation.Battle;
import java.io.File;
import java.io.IOException;

/**
 * Class used for operating all the programs' functionality,
 * used as a connector between backend and frontend.
 *
 * @author johnfb
 * @version 1.0
 */
public class Admin {
    private static Army homeArmy;
    private static Army awayArmy;
    private static Battle battle;

    //-------- Temporary variables ---------\\
    private static Army army;
    private static Army homeArmyBackup;
    private static Army awayArmyBackup;

    /**
     * Loads and sets the home-army from a file
     * @param file File to read
     * @throws IOException If an error occurred while reading the file
     */
    public static void loadHomeArmy(File file) throws IOException {
        homeArmy = FileManager.readArmy(file);
    }

    /**
     * Loads and sets the away-army from a file
     * @param file File to read
     * @throws IOException If an error occurred while reading the file
     */
    public static void loadAwayArmy(File file) throws IOException {
        awayArmy = FileManager.readArmy(file);
    }

    /**
     * Creates an army and saves it onto a file
     * @param file File to be created
     * @param armyName Name of the army
     * @param infaUnits Number of infantry units
     * @param rangUnits Number of ranged units
     * @param cavaUnits Number of cavalry units
     * @param commUnits Number of commander units
     * @throws IOException if any inputs are invalid
     */
    public static void saveHomeArmy(File file, String armyName, int infaUnits, int rangUnits, int cavaUnits, int commUnits) throws IOException {
        setHomeArmy(armyName, infaUnits, rangUnits, cavaUnits, commUnits);
        FileManager.writeArmyToFile(army, file);
    }

    /**
     * Saves the home army onto a file
     * @param file File to be created
     * @throws IOException if any inputs are invalid
     */
    public static void saveHomeArmy(File file) throws IOException {
        FileManager.writeArmyToFile(homeArmy, file);
    }

    /**
     * Creates an army and saves it onto a file
     * @param file File to be created
     * @param armyName Name of the army
     * @param infaUnits Number of infantry units
     * @param rangUnits Number of ranged units
     * @param cavaUnits Number of cavalry units
     * @param commUnits Number of commander units
     * @throws IOException if any inputs are invalid
     */
    public static void saveAwayArmy(File file, String armyName, int infaUnits, int rangUnits, int cavaUnits, int commUnits) throws IOException {
        setAwayArmy(armyName, infaUnits, rangUnits, cavaUnits, commUnits);
        FileManager.writeArmyToFile(army, file);
    }

    /**
     * Saves the away army onto a file
     * @param file File to be created
     * @throws IOException if any inputs are invalid
     */
    public static void saveAwayArmy(File file) throws IOException {
        FileManager.writeArmyToFile(awayArmy, file);
    }

    /**
     * @return Home-army
     */
    public static Army getHomeArmy() {
        return homeArmy;
    }

    /**
     * @return Away-army
     */
    public static Army getAwayArmy() {
        return awayArmy;
    }

    /**
     * @return Current battle
     */
    public static Battle getBattle() {
        return battle;
    }

    /**
     * Sets the home army
     * @param armyName Name of the army
     * @param infaUnits Number of infantry units
     * @param rangUnits Number of ranged units
     * @param cavaUnits Number of cavalry units
     * @param commUnits Number of commander units
     */
    public static void setHomeArmy(String armyName, int infaUnits, int rangUnits, int cavaUnits, int commUnits) {
        if (homeArmy != null) {
            if (homeArmy.getInfantryUnits().size() == infaUnits && homeArmy.getRangedUnits().size() == rangUnits &&
                    homeArmy.getCavalryUnits().size() == cavaUnits && homeArmy.getCommanderUnits().size() == commUnits) {
                return;
            }
        }
        setNewArmy(true, armyName, infaUnits, rangUnits, cavaUnits, commUnits);
    }

    /**
     * Sets the away army
     * @param armyName Name of the army
     * @param infaUnits Number of infantry units
     * @param rangUnits Number of ranged units
     * @param cavaUnits Number of cavalry units
     * @param commUnits Number of commander units
     */
    public static void setAwayArmy(String armyName, int infaUnits, int rangUnits, int cavaUnits, int commUnits) {
        if (awayArmy != null) {
            if (awayArmy.getInfantryUnits().size() == infaUnits && awayArmy.getRangedUnits().size() == rangUnits &&
            awayArmy.getCavalryUnits().size() == cavaUnits && awayArmy.getCommanderUnits().size() == commUnits) {
                return;
            }
        }
        setNewArmy(false, armyName, infaUnits, rangUnits, cavaUnits, commUnits);
    }

    /**
     * Creates a new battle with both armies
     * @param terrain The terrain the battle takes place in
     * @throws IllegalArgumentException Gives error if any of the armies are invalid
     */
    public static void startBattle(String terrain) throws IllegalArgumentException {
        homeArmyBackup = new Army(homeArmy);
        awayArmyBackup = new Army(awayArmy);
        battle = new Battle(homeArmy, awayArmy, terrain);
    }

    /**
     * Method for redoing a battle,
     * Creates a new battle using the backup-armies.
     * Because of randomness in a battle, the outcome will be different
     */
    public static void redoBattle() {
        homeArmy = new Army(homeArmyBackup);
        awayArmy = new Army(awayArmyBackup);
        battle = new Battle(homeArmy, awayArmy, battle.getTerrain().toString());
    }

    /**
     * Creates a new army and sets the new army as
     * either the home-army or away-army
     * @param isHomeArmy True if the home-army is going to be set
     * @param armyName Name of the army
     * @param infaUnits Number of infantry units
     * @param rangUnits Number of ranged units
     * @param cavaUnits Number of cavalry units
     * @param commUnits Number of commander units
     */
    public static void setNewArmy(boolean isHomeArmy, String armyName, int infaUnits, int rangUnits, int cavaUnits, int commUnits) {
        army = new Army(armyName);
        army.addAll(UnitFactory.createUnits("InfantryUnit", "Infantry", 100, infaUnits));
        army.addAll(UnitFactory.createUnits("RangedUnit", "Ranged", 80, rangUnits));
        army.addAll(UnitFactory.createUnits("CavalryUnit", "Cavalry", 110, cavaUnits));
        army.addAll(UnitFactory.createUnits("CommanderUnit", "Commander", 120, commUnits));

        if (isHomeArmy) {
            homeArmy = new Army(army);
        }
        else {
            awayArmy = new Army(army);
        }
    }
}
