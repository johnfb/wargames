package edu.ntnu.idatt2001.johnfb.wargames.guiControllers;

import edu.ntnu.idatt2001.johnfb.wargames.objects.observers.BattleObserver;
import edu.ntnu.idatt2001.johnfb.wargames.util.Admin;
import edu.ntnu.idatt2001.johnfb.wargames.util.SceneManager;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.util.Duration;

import java.io.IOException;

/**
 * Controller for the Battle-page
 * Used to simulate a battle
 * @author johnfb
 * @version 1.0
 */
public class BattleController {
    @FXML
    private Label titleLabel;
    @FXML
    private Label unitsLabelAway;
    @FXML
    private TextField numberOfRangedHome;
    @FXML
    private TextField numberOfInfantryAway;
    @FXML
    private TextField numberOfCommanderHome;
    @FXML
    private Label statusLabel;
    @FXML
    private TextField numberOfCavalryAway;
    @FXML
    private TextField numberOfCavalryHome;
    @FXML
    private TextField numberOfCommanderAway;
    @FXML
    private TextField numberOfRangedAway;
    @FXML
    private TextField numberOfInfantryHome;
    @FXML
    private ProgressBar statusBar;
    @FXML
    private Label unitsLabelHome;
    @FXML
    private BorderPane pane;
    @FXML
    private Button leaveBattleButton;

    private double winningPercentage = 0.0;
    private int homeUnits;
    private int awayUnits;
    private int count = 0;
    private Timeline timeline;
    private double updateInterval;

    /**
     * Method running on launch
     * Adds terrain values
     * Fills in army information
     * Calculates progressbar information
     */
    @FXML
    private void initialize() {
        leaveBattleButton.setDisable(true);
        setTerrainTheme();
        Admin.getBattle().simulate();

        System.out.println(BattleObserver.getHomeArmyHistory().size());
        //Change the update-interval, depending on how many updates need to be done
        if (BattleObserver.getHomeArmyHistory().size() < 500) {
            updateInterval = 0.05;
        }
        else if (BattleObserver.getHomeArmyHistory().size() < 2000) {
            updateInterval = 0.01;
        }
        else if (BattleObserver.getHomeArmyHistory().size() < 10000) {
            updateInterval = 0.001;
        }
        else {
            updateInterval = 0.0001;
        }

        timeline = new Timeline(new KeyFrame(Duration.seconds(updateInterval), e -> updateProgressBar()));

        update();
    }

    /**
     * Changes the background color depending on what terrain is selected
     */
    @FXML
    private void setTerrainTheme() {
        if (Admin.getBattle().getTerrain() == null) {
            return;
        }
        String terrain = Admin.getBattle().getTerrain().toString();
        if (terrain.equalsIgnoreCase("PLAINS")) {
            pane.setBackground(new Background(new BackgroundFill(Color.web("#caedd3"), null,  null)));
        }
        else if (terrain.equalsIgnoreCase("HILL")) {
            pane.setBackground(new Background(new BackgroundFill(Color.web("#bfb9a6"), null,  null)));
        }
        else if (terrain.equalsIgnoreCase("FOREST")) {
            pane.setBackground(new Background(new BackgroundFill(Color.web("#87b582"), null,  null)));
        }
    }

    /**
     * Method used to fill in all updated
     * information about both armies in the battle
     */
    @FXML
    private void update() {
        homeUnits = Admin.getHomeArmy().getAllUnits().size();
        awayUnits = Admin.getAwayArmy().getAllUnits().size();

        titleLabel.setText(Admin.getHomeArmy().getName() + " vs " + Admin.getAwayArmy().getName());
        numberOfInfantryHome.setText(Admin.getHomeArmy().getInfantryUnits().size() + "");
        numberOfRangedHome.setText(Admin.getHomeArmy().getRangedUnits().size() + "");
        numberOfCavalryHome.setText(Admin.getHomeArmy().getCavalryUnits().size() + "");
        numberOfCommanderHome.setText(Admin.getHomeArmy().getCommanderUnits().size() + "");
        numberOfInfantryAway.setText(Admin.getAwayArmy().getInfantryUnits().size() + "");
        numberOfRangedAway.setText(Admin.getAwayArmy().getRangedUnits().size() + "");
        numberOfCavalryAway.setText(Admin.getAwayArmy().getCavalryUnits().size() + "");
        numberOfCommanderAway.setText(Admin.getAwayArmy().getCommanderUnits().size() + "");

        //Creates a timeline inorder to run a function over time
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();

        leaveBattleButton.setDisable(false);
    }

    /**
     * Method used to calculate and set the
     * progress of the progressbar
     */
    @FXML
    private void updateProgressBar() {
        if (count == BattleObserver.getHomeArmyHistory().size()) {
            timeline.stop();
            return;
        }
        homeUnits = BattleObserver.getHomeArmyHistory().get(count);
        awayUnits = BattleObserver.getAwayArmyHistory().get(count);
        count++;
        unitsLabelHome.setText(homeUnits + "");
        unitsLabelAway.setText(awayUnits + "");
        winningPercentage = (double) homeUnits / (homeUnits + awayUnits);
        statusBar.setProgress(winningPercentage);

        //Update the status label
        if (homeUnits == 0 || awayUnits == 0) { //If someone has won
            if (winningPercentage == 0.5) {
                statusLabel.setText("It ended with a draw");
            }
            else if (winningPercentage > 0.5) {
                statusLabel.setText(Admin.getHomeArmy().getName() + " won!");
            }
            else if (winningPercentage < 0.5) {
                statusLabel.setText(Admin.getAwayArmy().getName() + " won!");
            }
        }
        else { //If the battle is still going on
            if (winningPercentage == 0.5) {
                statusLabel.setText("It's even");
            }
            else if (winningPercentage > 0.5) {
                statusLabel.setText(Admin.getHomeArmy().getName() + " is winning!");
            }
            else if (winningPercentage < 0.5) {
                statusLabel.setText(Admin.getAwayArmy().getName() + " is winning!");
            }
        }
    }

    @FXML
    public void leaveBattle(ActionEvent actionEvent) throws IOException {
        SceneManager.setView("main");
    }

    @FXML
    public void redoBattle(ActionEvent actionEvent) throws IOException {
        Admin.redoBattle();
        SceneManager.setView("battle");
    }
}
