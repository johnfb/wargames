package edu.ntnu.idatt2001.johnfb.wargames.guiControllers;

import edu.ntnu.idatt2001.johnfb.wargames.objects.simulation.Terrain;
import edu.ntnu.idatt2001.johnfb.wargames.util.Admin;
import edu.ntnu.idatt2001.johnfb.wargames.util.SceneManager;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import java.awt.*;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

/**
 * Controller for the AdvancedView-page
 * Used to edit individual units
 * @author johnfb
 * @version 1.0
 */
public class AdvancedViewController {
    @FXML
    private TextField unitHealth;
    @FXML
    private ChoiceBox terrainBox;
    @FXML
    private ListView<String> armyListAway;
    @FXML
    private ListView<String> armyListHome;
    @FXML
    private TextField unitName;
    @FXML
    private Label awayArmySource;
    @FXML
    private TextField armyNameHome;
    @FXML
    private Label homeArmySource;
    @FXML
    private Label errorLabel;
    @FXML
    private TextField armyNameAway;
    @FXML
    private Button saveChangesBtn;

    private boolean homeArmySelected;

    /**
     * Method running on launch
     * Adds terrain values
     * Fills in army information
     */
    @FXML
    public void initialize() {
        for (Terrain terrain : Terrain.values()) {
            terrainBox.getItems().add(terrain.toString());
        }
        terrainBox.setValue(terrainBox.getItems().get(0));

        armyNameHome.setText(Admin.getHomeArmy().getName());
        armyNameAway.setText(Admin.getAwayArmy().getName());
        saveChangesBtn.setDisable(true);

        armyListHome.getSelectionModel().selectedItemProperty().addListener((arg0, arg1, arg2) -> selectedHomeUnit());

        armyListAway.getSelectionModel().selectedItemProperty().addListener((observableValue, terrain, t1) -> selectedAwayUnit());

        updateHomeUnits();
        updateAwayUnits();
    }

    /**
     * Saves the away-army
     */
    @FXML
    public void saveArmyAway(ActionEvent actionEvent) {
        try {
            FileDialog fileDialog = new FileDialog(new Frame(), "Save", FileDialog.SAVE);
            fileDialog.setFilenameFilter((dir, name) -> name.endsWith(".csv"));
            fileDialog.setVisible(true);
            System.out.println("File: " + fileDialog.getFile());
            Admin.saveAwayArmy(fileDialog.getFiles()[0]);
            awayArmySource.setText(fileDialog.getFiles()[0].getPath());
        } catch (Exception e) {
            errorLabel.setText(e.getMessage());
        }
    }

    /**
     * Saves the home-army
     */
    @FXML
    public void saveArmyHome(ActionEvent actionEvent) {
        try {
            FileDialog fileDialog = new FileDialog(new Frame(), "Save", FileDialog.SAVE);
            fileDialog.setFilenameFilter((dir, name) -> name.endsWith(".csv"));
            fileDialog.setVisible(true);
            System.out.println("File: " + fileDialog.getFile());
            Admin.saveHomeArmy(fileDialog.getFiles()[0]);
            homeArmySource.setText(fileDialog.getFiles()[0].getPath());
        } catch (Exception e) {
            errorLabel.setText(e.getMessage());
        }
    }

    /**
     * Loads an away-army from file
     */
    @FXML
    public void loadArmyAway(ActionEvent actionEvent) {
        FileDialog dialog = new FileDialog((Frame)null, "Select File to Open");
        dialog.setMode(FileDialog.LOAD);
        dialog.setVisible(true);
        try {
            Admin.loadAwayArmy(dialog.getFiles()[0]);
            awayArmySource.setText(dialog.getFiles()[0].getPath());
            armyNameAway.setText(Admin.getAwayArmy().getName());
            updateAwayUnits();
        } catch (IOException e) {
            errorLabel.setText(e.getMessage());
        }
    }

    /**
     * Loads a home-army from file
     */
    @FXML
    public void loadArmyHome(ActionEvent actionEvent) {
        FileDialog dialog = new FileDialog((Frame)null, "Select File to Open");
        dialog.setMode(FileDialog.LOAD);
        dialog.setVisible(true);
        try {
            Admin.loadHomeArmy(dialog.getFiles()[0]);
            homeArmySource.setText(dialog.getFiles()[0].getPath());
            armyNameHome.setText(Admin.getHomeArmy().getName());
            updateHomeUnits();
        } catch (IOException e) {
            errorLabel.setText(e.getMessage());
        }
    }

    /**
     * Go to the main-page
     */
    @FXML
    public void simpleView(ActionEvent actionEvent) throws IOException {
        SceneManager.setView("main");
    }

    /**
     * Start a battle
     */
    @FXML
    public void startBattle(ActionEvent actionEvent) throws IOException {
        Admin.startBattle(terrainBox.getValue().toString());
        SceneManager.setView("battle");
    }

    /**
     * Updates the home-units list
     */
    @FXML
    public void updateHomeUnits() {
        armyListHome.getItems().removeAll(armyListHome.getItems());
        armyListHome.getItems().addAll(Admin.getHomeArmy().getAllUnitStrings());
    }

    /**
     * Updates the away-units list
     */
    @FXML
    public void updateAwayUnits() {
        armyListAway.getItems().removeAll(armyListAway.getItems());
        armyListAway.getItems().addAll(Admin.getAwayArmy().getAllUnitStrings());
    }

    /**
     * Method that runs every time the user has selected a unit from the home-army list.
     */
    @FXML
    public void selectedHomeUnit() {
        int index = armyListHome.getSelectionModel().getSelectedIndex();
        System.out.println(index);
        unitName.setText(Admin.getHomeArmy().getAllUnits().get(index).getName());
        unitHealth.setText(Admin.getHomeArmy().getAllUnits().get(index).getHealth() + "");
        homeArmySelected = true;
        saveChangesBtn.setDisable(false);
    }

    /**
     * Method that runs every time the user has selected a unit from the away-army list.
     */
    @FXML
    public void selectedAwayUnit() {
        int index = armyListAway.getSelectionModel().getSelectedIndex();
        System.out.println(index);
        unitName.setText(Admin.getAwayArmy().getAllUnits().get(index).getName());
        unitHealth.setText(Admin.getAwayArmy().getAllUnits().get(index).getHealth() + "");
        homeArmySelected = false;
        saveChangesBtn.setDisable(false);
    }

    /**
     * Saves the new unit information
     */
    @FXML
    public void saveChanges(ActionEvent actionEvent) {
        int health;
        String name = unitName.getText();

        try {
            health = Integer.parseInt(unitHealth.getText());
            if (health < 1) {
                throw new IllegalArgumentException("The health must be more than zero");
            }
        } catch (Exception e) {
            errorLabel.setText("You must enter a valid health-stat");
            return;
        }

        if (homeArmySelected) {
            try {
                Admin.getHomeArmy().getAllUnits().get(armyListHome.getSelectionModel().getSelectedIndex()).setName(name);
                Admin.getHomeArmy().getAllUnits().get(armyListHome.getSelectionModel().getSelectedIndex()).setHealth(health);
                SceneManager.setView("advancedView");
            } catch (Exception e) {
                errorLabel.setText(e.getMessage());
            }
        } else {
            try {
                Admin.getAwayArmy().getAllUnits().get(armyListAway.getSelectionModel().getSelectedIndex()).setName(name);
                Admin.getAwayArmy().getAllUnits().get(armyListAway.getSelectionModel().getSelectedIndex()).setHealth(health);
                SceneManager.setView("advancedView");
            } catch (Exception e) {
                errorLabel.setText(e.getMessage());
            }
        }
    }
}
