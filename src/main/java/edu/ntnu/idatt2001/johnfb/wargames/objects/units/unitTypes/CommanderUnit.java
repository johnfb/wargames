package edu.ntnu.idatt2001.johnfb.wargames.objects.units.unitTypes;

/**
 * CommanderUnit-class for creating a new Cavalry of the type Commander.
 *
 * @author johnfb
 * @version 1.0
 */
public class CommanderUnit extends CavalryUnit {

    /**
     * Constructor for the CommanderUnit-class.
     * Attack is locked at 25, and armor is locked at 15.
     * @param name the name of the CavalryUnit
     * @param health the health of the CavalryUnit
     */
    public CommanderUnit(String name, int health) throws IllegalArgumentException {
        super(name, health, 25, 15);
    }
}
