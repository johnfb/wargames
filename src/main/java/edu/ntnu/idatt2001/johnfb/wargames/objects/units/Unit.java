package edu.ntnu.idatt2001.johnfb.wargames.objects.units;

import java.util.Objects;

/**
 * Abstract Unit-class
 * Used for creating different types of units
 * @author johnfb
 * @version 1.0
 */
public abstract class Unit {
    private String name;
    private int health;
    private int attack;
    private int armor;

    /**
     * Constructor for the Unit-class
     * @param name the name of the unit
     * @param health the units health
     * @param attack the units attack-stat
     * @param armor the units armor
     */
    public Unit(String name, int health, int attack, int armor) throws IllegalArgumentException {
        if (attack < 0) {
            throw new IllegalArgumentException("A units attack-stat, can't be negative");
        }

        if (health < 0) {
            throw new IllegalArgumentException("A units health-stat, can't start out negative");
        }

        if (armor < 0) {
            throw new IllegalArgumentException("A units armor-stat, can't be negative");
        }

        if (name.isBlank()) {
            throw new IllegalArgumentException("A units name can't be blank");
        }

        this.name = name;
        this.health = health;
        this.attack = attack;
        this.armor = armor;
    }

    /**
     * Method for attacking another unit
     * health_opponent – (attack + attackBonus)_this + (armor + resistBonus)_opponent
     * @param opponent the unit that are being attacked
     */
    public void attack(Unit opponent, String terrain) {
        int newOpponentHealth = opponent.getHealth() - (this.getAttack() + this.getAttackBonus(terrain)) + (opponent.getArmor() + opponent.getResistBonus(terrain));
        opponent.setHealth(newOpponentHealth);
    }

    /**
     * Returns the name of a unit
     * @return String name
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the health of a unit
     * @return Int health
     */
    public int getHealth() {
        return health;
    }

    /**
     * Returns the attack of a unit
     * @return Int attack
     */
    public int getAttack() {
        return attack;
    }

    /**
     * Returns the armour of a unit
     * @return Int armour
     */
    public int getArmor() {
        return armor;
    }

    /**
     * Returns the attack bonus of a unit
     * @return Int attack bonus
     */
    public abstract int getAttackBonus(String terrain);

    /**
     * Returns the resist bonus of a unit
     * @return Int resist bonus
     */
    public abstract int getResistBonus(String terrain);

    /**
     * Sets the health of a unit
     * @param health New health value
     */
    public void setHealth(int health) {
        this.health = health;
    }

    /**
     * Changes the name of a unit
     * @param name New name to be set
     * @throws IllegalArgumentException Gives error if name is empty
     */
    public void setName(String name) throws IllegalArgumentException {
        if (name.isBlank()) {
            throw new IllegalArgumentException("A units name can't be blank");
        }
        this.name = name;
    }

    @Override
    public String toString() {
        return "Unit:\n" +
                "name=" + name + "\n" +
                "health=" + health + "\n" +
                "attack=" + attack + "\n" +
                "armor=" + armor + "\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Unit unit = (Unit) o;
        return health == unit.health && attack == unit.attack && armor == unit.armor && name.equals(unit.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, health, attack, armor);
    }
}
