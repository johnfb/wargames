module edu.ntnu.idatt2001.johnfb.wargames {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    requires javafx.base;
    requires java.desktop;

    opens edu.ntnu.idatt2001.johnfb.wargames to javafx.fxml;
    exports edu.ntnu.idatt2001.johnfb.wargames;
    opens edu.ntnu.idatt2001.johnfb.wargames.util to javafx.fxml;
    exports edu.ntnu.idatt2001.johnfb.wargames.util;
    opens edu.ntnu.idatt2001.johnfb.wargames.objects.simulation to javafx.fxml;
    exports edu.ntnu.idatt2001.johnfb.wargames.objects.simulation;
    opens edu.ntnu.idatt2001.johnfb.wargames.objects.units to javafx.fxml;
    exports edu.ntnu.idatt2001.johnfb.wargames.objects.units;
    opens edu.ntnu.idatt2001.johnfb.wargames.guiControllers to javafx.fxml;
    exports edu.ntnu.idatt2001.johnfb.wargames.guiControllers;
    opens edu.ntnu.idatt2001.johnfb.wargames.objects.observers to javafx.fxml;
    exports edu.ntnu.idatt2001.johnfb.wargames.objects.observers;
    exports edu.ntnu.idatt2001.johnfb.wargames.objects.units.unitTypes;
    opens edu.ntnu.idatt2001.johnfb.wargames.objects.units.unitTypes to javafx.fxml;
}